var gulp = require("gulp");
var postcss = require("gulp-postcss");
var sass = require("gulp-sass");
var sourcemaps = require("gulp-sourcemaps");
var uglify = require("gulp-uglify");
var autoprefixer = require("autoprefixer");
var cssnano = require("cssnano");
var browserSync = require("browser-sync");

sass.compiler = require("node-sass");

const sass2cssTask = () => {
  return gulp
    .src("./scss/**/*.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(gulp.dest("./css"));
};

const optimizeCSSTask = () => {
  var plugins = [
    autoprefixer(),
    cssnano({
      preset: [
        "default",
        {
          discardComments: {
            removeAll: true,
          },
          minifySelectors: false,
        },
      ],
    }),
  ];
  return gulp
    .src("./css/**/*.css")
    .pipe(sourcemaps.init())
    .pipe(postcss(plugins))
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest("./dist/css"));
};

const jsTask = () => {
  return gulp.src("./js/**/*.js").pipe(uglify()).pipe(gulp.dest("./dist/js"));
};

const fileChangeTask = () => {
  gulp.watch("./scss/**/*.scss", gulp.series(sass2cssTask, optimizeCSSTask));
  gulp.watch("./js/**/*.js", jsTask);
};

const browserSyncTask = () => {
  browserSync.init({
    server: {
      baseDir: ".",
    },
  });

  gulp.watch(["./*.html"]).on("change", browserSync.reload);
  gulp.watch(["./dist/css/**/*.css"]).on("change", browserSync.reload);
  gulp.watch(["./dist/js/**/*.js"]).on("change", browserSync.reload);
};

exports.build = gulp.series(sass2cssTask, optimizeCSSTask, jsTask);
exports.default = gulp.parallel(browserSyncTask, fileChangeTask);
