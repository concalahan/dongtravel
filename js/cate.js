$("#menu-click").click(function() {
    $("#myTopnav").toggleClass("responsive");
});

$("#blog-list-carousel").owlCarousel({
    nav:true,
    dots: false,
    items: 1,
    lazyLoad: true
});

$(document).ready(function () {
  
    var checkWidth = $(document).width();
    
    if(checkWidth <=600){
        $( "#list-item" ).addClass( "owl-carousel owl-theme" );
        $("#list-item").owlCarousel({
            items: 1,
            stagePadding: 50,
            dots: false,
            center: true
        });
    }
    else{
        $( "#list-item" ).removeClass( "owl-carousel owl-theme" );
    }
    
});
  