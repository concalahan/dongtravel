$ = jQuery;

var navbar = $("#navbar");

$(window).scroll(function () {
  if ($(window).scrollTop() > 0) {
    navbar.addClass("active");
  } else {
    navbar.removeClass("active");
  }

  if ($(window).scrollTop() > 200) {
    $("#back-to-top").addClass("active");
  } else {
    $("#back-to-top").removeClass("active");
  }

  // Hide on bottom desktop widget buy now
  if (
    $(".thuy").offset().top + $(".thuy").height() >=
    $("footer").offset().top
  ) {
    $(".thuy").css("visibility", "hidden");
  } else {
    $(".thuy").css("visibility", "visible");
  }
});

const postDetails = document.querySelector(".section");
const sidebar = document.querySelector(".tatcathuy");
const sidebarContent = document.querySelector(".tatcathuy > div");

//1
const controller = new ScrollMagic.Controller();

//2
const scene = new ScrollMagic.Scene({
  triggerElement: sidebar,
  triggerHook: 0.1,
  duration: getDuration,
}).addTo(controller);

//3
if (window.matchMedia("(min-width: 768px)").matches) {
  scene.setPin(sidebar, { pushFollowers: false });
}

//4
window.addEventListener("resize", () => {
  if (window.matchMedia("(min-width: 768px)").matches) {
    scene.setPin(sidebar, { pushFollowers: false });
  } else {
    scene.removePin(sidebar, true);
  }
});

function getDuration() {
  return postDetails.offsetHeight - (sidebarContent.offsetHeight + 500);
}

(function ($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
    if (
      location.pathname.replace(/^\//, "") ==
        this.pathname.replace(/^\//, "") &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
      if (target.length) {
        $("html, body").animate(
          {
            scrollTop: target.offset().top - 70,
          },
          500,
          "easeInOutExpo"
        );
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  // $('.js-scroll-trigger').click(function() {
  //     $('.navbar-collapse').collapse('hide');
  // });

  // Activate scrollspy to add active class to navbar items on scroll
  $("body").scrollspy({
    target: "#toc",
  });
})(jQuery); // End of use strict

// function scrollSection(index){
// 	var offsetClick = jQuery('#' + index ).offset().top;
//     jQuery('html, body').animate({
//         'scrollTop':offsetClick - 60
//     }, 300)
// }

//Accordion Hover
var acc = document.getElementsByClassName("accordion");
var i;
var a;

for (i = 0; i < acc.length; i++) {
  acc[0].classList.add("active");

  acc[i].addEventListener("mouseenter", function () {
    // this.classList.toggle("active");

    for (a = 0; a < acc.length; a++) {
      acc[a].classList.remove("active");
    }

    this.classList.add("active");
  });
}

$("#toole-sub").click(function () {
  $("#sub-menu").toggleClass("active");
});

$("#menu-click").click(function () {
  $("#myTopnav").toggleClass("responsive");
});

function openModal() {
  document.getElementById("myModal").style.display = "block";
}

function closeModal() {
  document.getElementById("myModal").style.display = "none";
}

function openImageModal() {
  document.getElementById("imageModal").style.display = "block";
}

function closeImageModal() {
  document.getElementById("imageModal").style.display = "none";
}
$("#back-to-top").on("click", function () {
  $("html, body").animate(
    {
      scrollTop: 0,
    },
    1000,
    "easeInOutExpo"
  );
});

function doCalculate() {
  var value_1 = document.getElementById("value-1").value; // A String value
  var value_2 = document.getElementById("value-2").value; // A String value
  var value_3 = document.getElementById("value-3").value; // A String value
  var price_1 =
    Number(value_1) * 1800000 +
    Number(value_2) * 1200000 +
    Number(value_3) * 100000;
  var del_1 =
    Number(value_1) * 2000000 +
    Number(value_2) * 1500000 +
    Number(value_3) * 200000;

  document.getElementById("price-1").innerHTML =
    price_1.toLocaleString() + " VND";
  document.getElementById("del-1").innerHTML = del_1.toLocaleString() + " VND";

  var value_4 = document.getElementById("value-4").value; // A String value
  var value_5 = document.getElementById("value-5").value; // A String value
  var value_6 = document.getElementById("value-6").value; // A String value
  var price_2 =
    Number(value_4) * 1800000 +
    Number(value_5) * 1200000 +
    Number(value_6) * 100000;
  var del_2 =
    Number(value_4) * 2000000 +
    Number(value_5) * 1500000 +
    Number(value_6) * 200000;

  document.getElementById("price-2").innerHTML =
    price_2.toLocaleString() + " VND";
  document.getElementById("del-2").innerHTML = del_2.toLocaleString() + " VND";

  var value_7 = document.getElementById("value-7").value; // A String value
  var value_8 = document.getElementById("value-8").value; // A String value
  var value_9 = document.getElementById("value-9").value; // A String value
  var price_3 =
    Number(value_7) * 1800000 +
    Number(value_8) * 1200000 +
    Number(value_9) * 100000;
  var del_3 =
    Number(value_7) * 2000000 +
    Number(value_8) * 1500000 +
    Number(value_9) * 200000;

  document.getElementById("price-3").innerHTML =
    price_3.toLocaleString() + " VND";
  document.getElementById("del-3").innerHTML = del_3.toLocaleString() + " VND";
}

// $('input[name=price]').on('change', function(){
//     doCalculate();
//     console.log('change');
// });

$(".plus-btn").click(function () {
  $(this)
    .prev()
    .val(+$(this).prev().val() + 1);
  doCalculate();
});
$(".minus-btn").click(function () {
  if ($(this).next().val() > 0)
    $(this)
      .next()
      .val(+$(this).next().val() - 1);
  doCalculate();
});

var read = document.getElementsByClassName("watch-more");
$(".watch-more").on("click", function () {
  $(".content").toggleClass("expand");
  for (var i = 0; i <= read.length; i++) {
    let text = $(read[i]).closest(".watch-more");

    if (text.text().trim() === "Xem thêm") {
      text.html("Rút gọn");
    } else {
      text.html("Xem thêm");
    }
  }
});

var image = document.getElementsByClassName("watch-more-image");
$(".watch-more-image").on("click", function () {
  $("#container").toggleClass("expand");
  // $('.wo-readmore').text('Rút gọn');
  for (var i = 0; i <= read.length; i++) {
    if (image[i].innerHTML === "Xem thêm") {
      image[i].html("Rút gọn");
    } else {
      image[i].html("Xem thêm");

      // image[i].innerHTML = "Xem thêm";
    }
  }
});

var readMorePrivacy = document.getElementsByClassName("watch-more-privacy");
$(".watch-more-privacy").on("click", function () {
  $(".privacy-inner").toggleClass("expand");

  for (var i = 0; i <= readMorePrivacy.length; i++) {
    let text = $(readMorePrivacy[i]).closest(".watch-more-privacy");
    if (text.text().trim() === "Xem thêm") {
      text.html("Rút gọn");
    } else {
      text.html("Xem thêm");
    }
  }
});

function openTab(evt, tabName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " active";
}

function openPrivacy(evt, tabName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("privacycontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("privacylinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" list1", "");
  }
  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " list1";
}

function openTour(evt, tabName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tourcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tourlinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " active";
}

function openComment(evt, tabName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("comment-wrap");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("comment-link");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " active";
}

function openLocation(evt, tabName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("location-wrap");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  // tablinks = document.getElementsByClassName("location-link");
  // for (i = 0; i < tablinks.length; i++) {
  //     tablinks[i].className = tablinks[i].className.replace(" active", "");
  // }
  document.getElementById(tabName).style.display = "block";
  // evt.currentTarget.className += " active";
}

$("#banner-slide").owlCarousel({
  loop: true,
  autoplay: true,
  autoWidth: true,
  margin: 0,
  nav: false,
  dots: false,
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 1,
    },
    1000: {
      items: 1,
    },
  },
});

$("#hot-slide").owlCarousel({
  loop: true,
  autoplay: true,
  autoWidth: true,
  margin: 0,
  nav: true,
  dots: false,
  items: 1,
  navText: [
    "<div class='nav-btn prev-slide'><i class='fas fa-chevron-left'></i></div>",
    "<div class='nav-btn next-slide'><i class='fas fa-chevron-right'></i></div>",
  ],
});

$("#location-slide").owlCarousel({
  loop: true,
  // autoplay: true,
  // autoWidth:true,
  margin: 15,
  nav: true,
  navText: [
    "<div class='nav-btn prev-slide'><i class='fas fa-chevron-left'></i></div>",
    "<div class='nav-btn next-slide'><i class='fas fa-chevron-right'></i></div>",
  ],
  dots: false,
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 3,
    },
    1000: {
      items: 5,
    },
  },
});

$("#trong-nuoc-slide1").owlCarousel({
  loop: false,
  rewind: true,
  autoplay: true,
  autoplayTimeout: 3000,
  // autoplayHoverPause:true,
  margin: 20,
  navigation: false, // Show next and prev buttons
  stagePadding: 20,
  item: 3,
  // navText: [
  //     "<div class='nav-btn prev-slide'><i class='fas fa-chevron-left'></i></div>",
  //     "<div class='nav-btn next-slide'><i class='fas fa-chevron-right'></i></div>"
  // ],
  dots: false,
  responsive: {
    0: {
      items: 2,
    },
    600: {
      items: 2,
    },
    1000: {
      items: 3,
    },
  },
});

$("#tour_tuong_tu").owlCarousel({
  loop: true,
  autoplay: true,
  items: 4,
  // autoWidth:true,
  margin: 10,
  nav: true,
  navText: [
    "<div class='nav-btn prev-slide'><i class='fas fa-chevron-left'></i></div>",
    "<div class='nav-btn next-slide'><i class='fas fa-chevron-right'></i></div>",
  ],
  dots: false,
  responsive: {
    0: {
      items: 2,
    },
    600: {
      items: 2,
    },
    1000: {
      items: 2,
    },
    1024: {
      items: 4,
    },
  },
});

$("#dich-vu-slide").owlCarousel({
  loop: true,
  autoplay: true,
  // autoWidth:true,
  margin: 15,
  nav: true,
  navText: [
    "<div class='nav-btn prev-slide'><i class='fas fa-chevron-left'></i></div>",
    "<div class='nav-btn next-slide'><i class='fas fa-chevron-right'></i></div>",
  ],
  dots: false,
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 3,
    },
    1000: {
      items: 4,
    },
  },
});

$("#am-thuc-slide").owlCarousel({
  loop: true,
  autoplay: true,
  // autoWidth:true,
  margin: 15,
  nav: true,
  navText: [
    "<div class='nav-btn prev-slide'><i class='fas fa-chevron-left'></i></div>",
    "<div class='nav-btn next-slide'><i class='fas fa-chevron-right'></i></div>",
  ],
  dots: false,
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 3,
    },
    1000: {
      items: 5,
    },
  },
});

$("#cam-hung-slide").owlCarousel({
  loop: true,
  autoplay: true,
  margin: 0,
  nav: true,
  navText: [
    "<div class='nav-btn prev-slide'><i class='fas fa-chevron-left'></i></div>",
    "<div class='nav-btn next-slide'><i class='fas fa-chevron-right'></i></div>",
  ],
  dots: false,
  items: 1,
});

$("#cam-nghi-slide11").owlCarousel({
  loop: true,
  // autoplay: true,
  margin: 0,
  nav: true,
  navText: [
    "<div class='nav-btn prev-slide'><i class='fas fa-chevron-left'></i></div>",
    "<div class='nav-btn next-slide'><i class='fas fa-chevron-right'></i></div>",
  ],
  dots: false,
  responsive: {
    0: {
      items: 3,
    },
    600: {
      items: 3,
    },
    1000: {
      items: 5,
    },
    1024: {
      items: 6,
    },
  },
});

$("#gioi-thieu-slide").owlCarousel({
  loop: true,
  autoplay: true,
  margin: 20,
  nav: true,
  navText: [
    "<div class='nav-btn prev-slide'><i class='fas fa-chevron-left'></i></div>",
    "<div class='nav-btn next-slide'><i class='fas fa-chevron-right'></i></div>",
  ],
  dots: false,
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 3,
    },
    1000: {
      items: 4,
    },
  },
});

$("#doi-tac-slide").owlCarousel({
  loop: true,
  autoplay: true,
  // autoWidth:true,
  margin: 15,
  nav: false,
  dots: false,
  responsive: {
    0: {
      items: 2,
    },
    600: {
      items: 4,
    },
    1000: {
      items: 8,
    },
  },
});

$("#modal-slide").owlCarousel({
  loop: true,
  autoplay: true,
  // autoWidth:true,
  margin: 15,
  nav: true,
  dots: false,
  items: 1,
});

function postsCarousel() {
  var checkWidth = $(window).width();
  var owlPost = $("#progressbar");
  var comment = $("#coppy7");
  if (checkWidth > 767) {
    if (typeof owlPost.data("owl.carousel") != "undefined") {
      owlPost.data("owl.carousel").destroy();
    }

    if (typeof comment.data("owl.carousel") != "undefined") {
      comment.data("owl.carousel").destroy();
      comment.removeClass("owl-carousel");
    }

    owlPost.removeClass("owl-carousel");
  } else if (checkWidth < 768) {
    owlPost.addClass("owl-carousel");
    comment.addClass("owl-carousel");

    owlPost.owlCarousel({
      items: 5,
      slideSpeed: 500,
      animateOut: "fadeOut",
      autoplay: false,
      autoplaySpeed: 2000,
      autoplayTimeout: 2000,
      dots: true,
      nav: true,
      navText: [
        "<div class='nav-btn prev-slide'><i class='fas fa-chevron-left'></i></div>",
        "<div class='nav-btn next-slide'><i class='fas fa-chevron-right'></i></div>",
      ],
      loop: false,
    });

    comment.owlCarousel({
      items: 2,
      slideSpeed: 500,
      animateOut: "fadeOut",
      autoplay: false,
      autoplaySpeed: 2000,
      autoplayTimeout: 2000,
      margin: 10,
      dots: true,
      nav: true,
      navText: [
        "<div class='nav-btn prev-slide'><i class='fas fa-chevron-left'></i></div>",
        "<div class='nav-btn next-slide'><i class='fas fa-chevron-right'></i></div>",
      ],
      loop: false,
    });
  }
}

postsCarousel();
$(window).resize(postsCarousel);

$("#feature-video").on("click", function (e) {
  console.log("QgFS3SC4xIA " + $("#feature-video").data("video-id"));

  $("#videoModal iframe").attr("src", $("#myModal iframe").attr("src"));
});

lightbox.option({
  resizeDuration: 200,
  wrapAround: true,
});

$(document).ready(function () {
  $("body").tooltip({ selector: "[data-toggle=tooltip]" });
});

$price_calc = document.querySelectorAll(".price-calc");

console.log($price_calc.length);

$tour_links = document.querySelectorAll(".tourlinks");

console.log($tour_links.length);

$tour_link_active = document.querySelectorAll(".tourlinks.active");

console.log($tour_link_active.length);

if ($tour_link_active.length < 1) {
  $tour_links[0].classList.add("active");

  $tour_links[0].click();
}
