$ = jQuery;

        var navbar = $('#navbar');

        $(window).scroll(function () {
            if ($(window).scrollTop() > 0) {
                navbar.addClass('active');

            } else {
                navbar.removeClass('active');
            }

            if($(window).scrollTop() > 200){
                $('#back-to-top').addClass('active');
            }else{
                $('#back-to-top').removeClass('active');
            }

        });

        $(document).ready(function () {
            $('.product-list').slick({
                infinite: false,
                slidesToShow: 2,
                responsive: [
                    {

                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2,
                        }

                    },
                    {

                        breakpoint: 540,
                        settings: {
                            slidesToShow: 1,
                        }

                    }
                ]
            })
            $('.post-slick').slick({
                infinite: false,
                slidesToShow: 4,
                responsive: [
                    {

                        breakpoint: 992,
                        settings: {
                            slidesToShow: 3,
                        }

                    },
                    {

                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2,
                        }

                    },
                    {

                        breakpoint: 540,
                        settings: {
                            slidesToShow: 2,
                        }

                    }
                ]
            })
            if (window.matchMedia("(max-width: 768px)").matches) {
                $('.review-container').slick({
                    slidesToShow: 2,
                    infinite: false,
                    responsive: [
                        {

                            breakpoint: 768,
                            settings: {
                                slidesToShow: 2,
                            }

                        },
                        {

                            breakpoint: 540,
                            settings: {
                                slidesToShow: 1,
                            }

                        }
                    ]
                })
            }

        })